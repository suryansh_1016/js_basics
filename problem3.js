// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortAlphabetically(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Array not found");
    return;
  }

  if (inventory.length == 0) {
    console.log("Array is empty");
    return;
  } else {
    inventory.sort((a, b) => a.car_model.localeCompare(b.car_model));
    for (let index = 0; index < inventory.length; index++) {
      console.log(inventory[index].car_model);
    }
  }
}

module.exports = sortAlphabetically;
