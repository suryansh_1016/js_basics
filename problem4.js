// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function returnCarYears(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Array not found");
    return;
  }

  if (inventory.length == 0) {
    console.log("Array is empty");
    return;
  } else {
    const result = [];
    for (let index = 0; index < inventory.length; index++) {
      result.push(inventory[index].car_year);
    }
    return result;
  }
}

module.exports = returnCarYears;
