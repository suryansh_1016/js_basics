let inventory = require("../inventory.js");
const returnCarYears = require("../problem4.js");
const returnOldCars = require("../problem5");

const yearData = returnCarYears(inventory);

const result = returnOldCars(inventory, yearData);
