// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

function returnLastCar(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Array not found");
    return;
  }

  if (inventory.length == 0) {
    return console.log("Array is empty");
  } else {
    console.log(
      `Last car is a ${inventory[inventory.length - 1].car_make} ${
        inventory[inventory.length - 1].car_model
      }`
    );
  }
}

module.exports = returnLastCar;
